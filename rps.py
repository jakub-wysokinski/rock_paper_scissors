from random import randint


class Choosen():
    """
    Class creates Rock Paper Scissors instance, where variable is choosen.
    Amount of variables (Rock Paper Scissors etc.) is infinite, however hardcoded in 'options'
    amount of options has to be odd, because for each variable there has to be same number
    of variables against which it wins and looses.

    Method find_value, finds name of the variable when given is its number

    Init is also checking the correctness of passed element, and validates it.
    available variables:
        :var_num = Number of variable in options
        :var = Name of variable
        :num_of_options = length of options

    """

    options_3 = {0: "ROCK", 1: "SCISSORS", 2: "PAPER"}

    options_15 = {0: "ROCK", 1: "FIRE", 2: "SCISSORS", 3: "SNAKE", 4: "HUMAN", 5: "TREE",
                  6: "WOLF", 7: "SPONGE", 8: "PAPER", 9: "AIR", 10: "WATER", 11: "DRAGON",
                  12: "DEVIL", 13: "LIGHTNING", 14: "GUN"}

    options_101 = {0: "DYNAMITE", 1: "TORNADO", 2: "QUICKSAND", 3: "PIT", 4: "CHAIN", 5: "GUN", 6: "LAW", 7: "WHIP",
                   8: "SWORD", 9: "ROCK", 10: "DEATH", 11: "WALL", 12: "SUN", 13: "CAMERA", 14: "FIRE", 15: "CHAINSAW",
                   16: "SCHOOL", 17: "SCISSORS", 18: "POISON", 19: "CAGE", 20: "AXE", 21: "PEACE", 22: "COMPUTER",
                   23: "CASTLE", 24: "SNAKE", 25: "BLOOD", 26: "PORCUPINE", 27: "VULTURE", 28: "MONKEY", 29: "KING",
                   30: "QUEEN", 31: "PRINCE", 32: "PRINCESS", 33: "POLICE", 34: "WOMAN", 35: "BABY", 36: "MAN",
                   37: "HOME", 38: "TRAIN", 39: "CAR", 40: "NOISE", 41: "BICYCLE", 42: "TREE", 43: "TURNIP", 44: "DUCK",
                   45: "WOLF", 46: "CAT", 47: "BIRD", 48: "FISH", 49: "SPIDER", 50: "COCKROACH", 51: "BRAIN",
                   52: "COMMUNITY", 53: "CROSS", 54: "MONEY", 55: "VAMPIRE", 56: "SPONGE", 57: "CHURCH", 58: "BUTTER",
                   59: "BOOK", 60: "PAPER", 61: "CLOUD", 62: "AIRPLANE", 63: "MOON", 64: "GRASS", 65: "FILM",
                   66: "TOILET", 67: "AIR", 68: "PLANET", 69: "GUITAR", 70: "BOWL", 71: "CUP", 72: "BEER", 73: "RAIN",
                   74: "WATER", 75: "TV", 76: "RAINBOW", 77: "UFO", 78: "ALIEN", 79: "PRAYER", 80: "MOUNTAIN",
                   81: "SATAN", 82: "DRAGON", 83: "DIAMOND", 84: "PLATINUM", 85: "GOLD", 86: "DEVIL", 87: "FENCE",
                   88: "VIDEO GAME", 89: "MATH", 90: "ROBOT", 91: "HEART", 92: "ELECTRICITY", 93: "LIGHTNING",
                   94: "MEDUSA", 95: "POWER", 96: "LASER", 97: "NUKE", 98: "SKY", 99: "TANK", 100: "HELICOPTER"}

    options = options_101
    score = 0

    @classmethod
    def find_value(cls, variable):
        """
        Finds in options key number, while given is value.
        """
        reversed_options = {v: k for k, v in cls.options.items()}
        return reversed_options[(variable.upper())]

    def __init__(self, variable):
        # checking if number or string was passed else it is an error
        if isinstance(variable, int):
            self.var_num: int = variable
            try:
                self.var: str = self.options[self.var_num]
                self.options_len: int = len(self.options)
            except KeyError as err:
                # given was number which does not exists in available options
                raise KeyError("No such option available")
        elif isinstance(variable, str):
            self.var_num: str = self.find_value(variable)
            if self.var_num is not None:
                self.var: str = self.options[self.var_num]
                self.options_len: int = len(self.options)
            else:
                # given was string which does not exists in available options
                raise KeyError("No such option available")
        else:
            # given was value which is not supported
            raise KeyError("No such option available, this type is not supported")

    def __repr__(self):
        return (self.var)

    def __eq__(self, other):
        return self.play(other)

    def play_with_computer(self):
        self.compare(Choosen(randint(self.options_len)))

    def play(self, other):
        difference = self.var_num - other.var_num
        if difference == 0:
            # if the difference is 0 it means there were used same signs
            print("It is a TIE")
            return True
        elif abs(difference) > (self.options_len / 2):
            # absolute difference between values is bigger than half of available options
            if (self.var_num + difference) > self.options_len:
                # The value got over available options what makes it is taken from first options or last options
                # but still in field where it wins
                print("You WIN")
                self.score += 1
                other.score -= 1
                return True
            else:
                # The value got over available options what makes it is taken from first options or last options
                # but it looses or the difference in values is so big that the mode has changed
                print("You Lost")
                self.score -= 1
                other.score += 1
                return False
        else:
            # Values where difference does not go beyond maximal or minimal values
            if difference < 0:
                # difference is smaller than 0 what means is negative what results in winning
                print("You WIN")
                self.score += 1
                other.score -= 1
                return True
            else:
                # difference is bigger than 0 what means is positive what results in loosing
                print("You Lost")
                self.score -= 1
                other.score += 1
                return False
