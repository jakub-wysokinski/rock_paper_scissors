import unittest
from rps import Choosen


class ChoosenFindValue(unittest.TestCase):
    def test_correct_value(self):
        self.assertEqual(Choosen.find_value("BLOOD"), 25)

    def test_lower_case_correct_value(self):
        self.assertEqual(Choosen.find_value("blood"), 25)

    def test_correct_value_lowest(self):
        self.assertEqual(Choosen.find_value("DYNAMITE"), 0)

    def test_correct_value_highest(self):
        self.assertEqual(Choosen.find_value("HELICOPTER"), 100)

    def test_incorrect_value(self):
        self.assertNotEqual(Choosen.find_value("HELICOPTER"), 10)

    def test_not_existing_value(self):
        with self.assertRaises(KeyError):
            Choosen.find_value("TEST")

    def test_integer(self):
        with self.assertRaises(AttributeError):
            Choosen.find_value(1)

    def test_empty_list(self):
        with self.assertRaises(AttributeError):
            Choosen.find_value([])

    def test_list_with_integers(self):
        with self.assertRaises(AttributeError):
            Choosen.find_value([1,2,3])

    def test_list_with_strings(self):
        with self.assertRaises(AttributeError):
            Choosen.find_value(["HELICOPTER"])

    def test_no_attribute(self):
        with self.assertRaises(TypeError):
            Choosen.find_value()

class ChoosenPlay(unittest.TestCase):
    def the_play(self, first_player_value, second_player_value, first_player_expected_score,
                 second_player_expected_score):
        first_player = Choosen(first_player_value)
        second_player = Choosen(second_player_value)
        first_player == second_player
        self.assertEqual(first_player.score, first_player_expected_score)
        self.assertEqual(second_player.score, second_player_expected_score)

    def test_tie_lowest(self):
        self.the_play(0, 0, 0, 0)

    def test_tie(self):
        self.the_play(30, 30, 0, 0)

    def test_tie_highest(self):
        self.the_play(100, 100, 0, 0)

    def test_lowest_for_first(self):
        self.the_play(0, 1, 1, -1)

    def test_middle_for_first(self):
        self.the_play(0, 25, 1, -1)

    def test_highest_for_first(self):
        self.the_play(0, 50, 1, -1)

    def test_lowest_for_last(self):
        self.the_play(100, 0, 1, -1)

    def test_middle_for_last(self):
        self.the_play(100, 24, 1, -1)

    def test_highest_for_last(self):
        self.the_play(100, 49, 1, -1)

    def test_lowest_for_middle(self):
        self.the_play(50, 51, 1, -1)

    def test_highest_for_middle(self):
        self.the_play(50, 100, 1, -1)

if __name__ == '__main__':
    unittest.main()